<?php

namespace joyqhs\Amazon;

use ClouSale\AmazonSellingPartnerAPI\Api\FbaInboundApi;
use ClouSale\AmazonSellingPartnerAPI\ApiException;
use ClouSale\AmazonSellingPartnerAPI\Configuration;
use ClouSale\AmazonSellingPartnerAPI\Models\FulfillmentInbound\GetLabelsResponse;
use ClouSale\AmazonSellingPartnerAPI\ObjectSerializer;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class ZhimuFbaInboundApi extends FbaInboundApi
{
    public function __construct(Configuration $config)
    {
        parent::__construct($config);
    }

    /**
     * Operation getLabels.
     *
     * @param string $shipment_id A shipment identifier originally returned by the createInboundShipmentPlan operation. (required)
     * @param string $page_type The page type to use to print the labels. Submitting a PageType value that is not supported in your marketplace returns an error. (required)
     * @param string $label_type The type of labels requested. (required)
     * @param int $number_of_packages The number of packages in the shipment. (optional)
     * @param string[] $package_labels_to_print A list of identifiers that specify packages for which you want package labels printed.  Must match CartonId values previously passed using the FBA Inbound Shipment Carton Information Feed. If not, the operation returns the IncorrectPackageIdentifier error code. (optional)
     * @param int $number_of_pallets The number of pallets in the shipment. This returns four identical labels for each pallet. (optional)
     *
     * @return \ClouSale\AmazonSellingPartnerAPI\Models\FulfillmentInbound\GetLabelsResponse
     * @throws \ClouSale\AmazonSellingPartnerAPI\ApiException on non-2xx response
     *
     * @throws \InvalidArgumentException
     */
    public function getLabels($shipment_id, $page_type, $label_type, $page_size, $number_of_packages = null, $package_labels_to_print = null, $number_of_pallets = null)
    {
        list($response) = $this->getLabelsWithHttpInfo($shipment_id, $page_type, $label_type, $page_size, $number_of_packages, $package_labels_to_print, $number_of_pallets);

        return $response;
    }

    /**
     * Operation getLabelsWithHttpInfo.
     *
     * @param string $shipment_id A shipment identifier originally returned by the createInboundShipmentPlan operation. (required)
     * @param string $page_type The page type to use to print the labels. Submitting a PageType value that is not supported in your marketplace returns an error. (required)
     * @param string $label_type The type of labels requested. (required)
     * @param int $number_of_packages The number of packages in the shipment. (optional)
     * @param string[] $package_labels_to_print A list of identifiers that specify packages for which you want package labels printed.  Must match CartonId values previously passed using the FBA Inbound Shipment Carton Information Feed. If not, the operation returns the IncorrectPackageIdentifier error code. (optional)
     * @param int $number_of_pallets The number of pallets in the shipment. This returns four identical labels for each pallet. (optional)
     *
     * @return array of \ClouSale\AmazonSellingPartnerAPI\Models\FulfillmentInbound\GetLabelsResponse, HTTP status code, HTTP response headers (array of strings)
     * @throws \ClouSale\AmazonSellingPartnerAPI\ApiException on non-2xx response
     *
     * @throws \InvalidArgumentException
     */
    public function getLabelsWithHttpInfo($shipment_id, $page_type, $label_type, $page_size, $number_of_packages = null, $package_labels_to_print = null, $number_of_pallets = null)
    {
        $request = $this->getLabelsRequest($shipment_id, $page_type, $label_type, $page_size, $number_of_packages, $package_labels_to_print, $number_of_pallets);

        return $this->sendRequest($request, GetLabelsResponse::class);
    }

    /**
     * @throws ApiException
     */
    private function sendRequest(Request $request, string $returnType): array
    {
        try {
            $options = $this->createHttpClientOption();
            try {
                $response = $this->client->send($request, $options);
            } catch (RequestException $e) {
                throw new ApiException("[{$e->getCode()}] {$e->getMessage()}", $e->getCode(), $e->getResponse() ? $e->getResponse()->getHeaders() : null, $e->getResponse() ? $e->getResponse()->getBody()->getContents() : null);
            }
            $statusCode = $response->getStatusCode();

            if ($statusCode < 200 || $statusCode > 299) {
                throw new ApiException(sprintf('[%d] Error connecting to the API (%s)', $statusCode, $request->getUri()), $statusCode, $response->getHeaders(), $response->getBody());
            }

            $responseBody = $response->getBody();

            if ('\SplFileObject' === $returnType) {
                $content = $responseBody; //stream goes to serializer
            } else {
                $content = $responseBody->getContents();
                if (!in_array($returnType, ['string', 'integer', 'bool'])) {
                    $content = json_decode($content);
                }
            }
//            var_dump($content);
//            exit();

            return [
                ObjectSerializer::deserialize($content, $returnType, []),
                $response->getStatusCode(),
                $response->getHeaders(),
            ];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 503:
                case 500:
                case 429:
                case 404:
                case 403:
                case 401:
                case 400:
                case 200:
                    $data = ObjectSerializer::deserialize(
                        $e->getResponseBody(),
                        $returnType,
                        $e->getResponseHeaders()
                    );
                    $e->setResponseObject($data);
                    break;
            }
            throw $e;
        }
    }
}